---
title: About me
#subtitle: Why you'd want to hang out with me
comments: false
---
## About
I'm a site reliability engineer, open-source advocate, who loves to ride bikes, and go hiking.

I specialize in the "Cloud", mainly AWS, but I've built and maintained systems in Azure and Google Cloud.  I'm a lover of Linux over Windows but Windows is a tool and has it's place in the tech world.  My day to day is building and maintaining systems in AWS, and implementing solutions that save everyone time and headache.  After work I'm usually riding my bicycle, hiking/camping or spending time with my wonderful kid. After he goes to bed I'm usually messing with new technology.

## Hobbies
* Open Source Software
* Technology
* Bicycling
* Outdoors

## Contact
* Email: me at cwiggs.com
* GPG fingerprint: 8777 8154 26B3 3D3E 1739 FFDD 9934 39CD 7F46 F139
