---
title: cv
subtitle: My resume/CV 
comments: false
---

# Skills
---
* AWS/Azure/GCP
* Linux Administration (Ubuntu/RHEL/Amazon Linux)
* Docker, Docker Swarm, Kubernetes
* Chef and Ansible
* MySQL, MSSQL, DynamoDB, etc
* Python, Javascript, & Bash
* Network administration (DHCP, DNS, QoS, VPCs)
* Windows server administration (2003, 2008, 2012, 2016)
* Virtualization (KVM, VMware, Hyper V)
* Computer hardware setup/troubleshooting
* VOIP systems (Asterisk, FreePBX)
* Atlassian tools, Scrum, Agile methodology.

# Experience
---
<h2 style="font-size:90%;"> Symantec </h2>
<p style="font-size:80%;" aligned=left> <b> Senior Site Reliability Engineer </b> <span style="float:right;"> Feb 2017 - Current </span></p>

* Successfully migrated all on-prem infrastucture to AWS using a new CI/CD pipeline on time.
* Responsible for building & architecting a big data warehouse in Azure.
* Built various infrastruce using Terraform and configuring the software using Chef cookbooks.
* Manage ECS environment that uses ASG, LB, for a true HA setup.
* Help CloudOps team get ready for migration from ESXI to AWS.
* Patch 1000s of Linux systems using Red Hat’s Satellite on a monthly basis, as well as patch ESXI hosts.
* Remideate various software exploits using a custom in-house vulnabilaty remediation software.

<h2 style="font-size:90%;"> Newtek Technologies </h2>
<p style="font-size:80%;" aligned=left> <b> Systems Administrator </b> <span style="float:right;"> March 2016 - Feb 2017 </span></p>

* Work with customers to setup and maintain their Windows or Linux enviornments, as well as Sophos UTM/XG and Sonicwalls.
* Deploy Windows and Linux Shared environments, such as installing and configurging Cpanel, CSF/iptables, Apache, Ansible, IIS, SmarterMail, MySQL/MSSQL.
* Maintain Windows and Linux Shared environments, for example: keep mail spool low on Exim and SmarterMail, remove compromised accounts/files from mail, and/or cpanel accounts, as well as fix various software issues.

<h2 style="font-size:90%;"> Hyperion Works </h2>
<p style="font-size:80%;" aligned=left> <b> Linux Specialist </b> <span style="float:right;"> September 2013 - March 2016 </span></p>

* Responsible for multiple Ubuntu/Debian domain controllers (Samba 3/4) and file servers (CIFS, NFS, SSHFS) with MDADM RAID arrays and LVM.
* Deploy and maintain networks (DHCP, DNS, QoS, OpenWRT, OpenVPN, etc).
* Provide on-site support, remote (VNC, RDP) support, and maintenance (software updates and security patches).
* Responsible for off-site and on-site backups using Rsnapshot and Acronis.
* Configure and maintain VOIP systems (FreePBX, Asterisk), as well as the soft/hard phones.

<h2 style="font-size:90%;"> Ubuntu Arizona LOCO </h2>
<p style="font-size:80%;" aligned=left> <b> Volunteer </b> <span style="float:right;"> December 2007 - Current </span></p>

Installation of Ubuntu Linux on various machines, as well as troubleshooting any problems with the new install. Troubleshooting of numerous customer's personal Linux systems. Also helped with coordinating Installfests.

# Education
---
<h2 style="font-size:90%;"> Heat Sync Labs </h2>
May 2012 - Current

* Focus on PHP, Python, SQL
* Rasberry Pi Projects

