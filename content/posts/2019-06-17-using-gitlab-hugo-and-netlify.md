---
title: Setup site with Gitlab Hugo and Netlify 
date: 2019-06-17
tags: ["blog", "site", "iac"]
---

# This site
This site is setup using [hugo](https://gohugo.io/) and being stored at Gitlab, and then hosted using 
[netlify](https://www.netlify.com/).  The guide I used is 
[here](https://brainfood.xyz/post/20190518-host-your-own-blog-in-1-hour/), it's an interesting read.

# Advantages
## Infrastructure
The cool thing about this setup is there is no infrastructure that you have to manage.
Before this iteration of my site I had it hosted on a VM at GCP and used [grav](http://getgrav.org/).
Thanks to the always-free tier of GCP I didn't pay anything, but I still had to manage the VM.
Now I focus one the code that's all stored in git.

## No Backups
Since all the code for this site is stored in git, I don't have to keep backups.  If something happens and the site goes down, I just git clone the repo to a new server and it's up and running.

## Deployments
The deploysments using this setup is much easier as well.  I just make all my changes in the `develop` branch,
 make sure they work locally using `hugo server`, push the develop branch up and test the deployment in develop
on netlify. Then once I'm happy with my changes I open a PR and merge to master.  Netlify then sees my new
commit in master and automatically deploys the site.

# Misc thoughts
The nice thing about grav is that it might be a little more user friendly for non-tech users since it has an admin gui.
Grav still uses markdown, which is a major thing I wanted, however it's just a flat-file system CMS and therefore you
can't host as easily as you can a static site.

The guide I followed to setup this site did talk about using the Gitlab web IDE which could make the site
easier for non-tech people, but you'd still have to explain a little to them about git, Gitlab, and the limitations
of static sites.  It'd be interesting to see a successful business that hosts sites using some type of static site generator.
