---
title: Jenkins Docker Image with JCasC Part 2
date: 2019-10-29
tags: ['docker', 'tech', 'jenkins', 'iac']
---

[Previously](/post/2019-08-15-docker-jenkins-jcasc) I wrote about a docker image that had
Jenkins and the [JCasC plugin](https://github.com/jenkinsci/configuration-as-code-plugin).

This post is about an update to that code with a few enhancements, such as:
* A job that runs daily that will apply the JCasC config, thus prevently drift.
* jenkins.yaml is split up into multiple yaml file and the entire dir is loaded.

You can view the code on [gitlab](https://gitlab.com/cwiggs/jenkins-docker-jcasc/tree/master).

## Dockerfile
The changes we made to the Dockerfile are simple using the `CASC_JENKINS_CONFIG`
environment variable to set a dir, and then copy all the yaml files in `casc_configs`
to that directory.  Now any yaml files you put in `casc_configs` will be loaded
via the JCasC plugin.

```
# Copy Jenkins config as code
ENV CASC_JENKINS_CONFIG /var/jenkins_home/casc_configs/
COPY casc_configs/* ${CASC_JENKINS_CONFIG}
```

## plugins.txt
I also added a few plugins to the `plugins.txt` file.  Namely job-dsl so we can create
the JCasC job that runs daily to re-apply the JCasC config.  Cloudbees-folder was added
so we can put the JCasC job in a folder. groovy was added so we can run the `groovySystemCommand`
in the JCasC job.  Finally i added script-security to verify it was on 1.66, pre 1.64 and the
plugin did not work with JCasC.

## JCasC yaml files
for the JCasC yaml configs, before we had only a jenkins.yaml.  Now we have `jenkins.yaml`,
`jobs.yaml`, and `security.yaml`, each for the top level yaml objects. `jenkins.yaml` is
just the jenkins top level yaml object, with no changes from the previous post. `jobs.yaml`
is where our JCasC job lives, and if we want to put future jobs there we can. Finally
`security.yaml` has the security settings, with an added `scriptApproval` section
that allows the JCasC groovy code to run without having to approve it manually.

### jobs.yaml
The jobs config uses job-dsl to create a folder and a job inside that folder.
The job uses the `systemGroovyCommand` argument to apply the JCasC config daily.

If you wanted to add more jobs you could add them in that file.  It might be 
better to create a seed job that pulls job-dsl from git and applies them.
The downside to seed job is the down-stream jobs won't exist until the 
seed job runs.

### security.yaml
The security file is adding 2 "signatures" to the scriptApproval plugin.  Without this
you would have to run the job, and go to `<jenkins_url>/scriptApproval` and manually
approve the job, then run the job again for it to work.

I did run into a bug with the `scriptApproval` method which caused me to restart Jenkins
before it would actually take effect.  
I opened [JENKINS-59979](https://issues.jenkins-ci.org/browse/JENKINS-59979) to track
the issue.
