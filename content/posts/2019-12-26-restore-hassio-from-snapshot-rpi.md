---
title: Restore Hass.io From Snapshot on a Raspberry Pi 4 
date: 2019-12-15
tags: ['rpi', 'raspberrpi', 'linux', iot, hassio, home-assistant]
---

I recently setup hass.io (home assistant OS) on a new RPI 4 that I bought.  I originally
set it up with a 16G SD card, but later upgraded to a 64G SD card for more space.
Searching online I couldn't find a very good guide on how to restore from a hass.io snapshot.
Here is a brief way it can be done using the ssh plugin:

1. Install the ssh plugin.
2. Add your public ssh key to the ssh plugin config and start the plugin.
3. Copy your snapshot to /backups. I used scp: `scp ~/Downloads/Hass_io_20191215.tar root@10.0.0.90:/backups`
4. SSH into the RPI and run `hassio sn reload` so hassio loads the new snapshot from the filesystem.
5. Now that the snapshot is loaded you can restore via the GUI, or run `hassio sn restore -slug Your_Snapshot_Slug_Name [-password Your_Snapshot_Password]` 

Hopefully that helps someone in the future, who wants a quick and dirty guide on how to restore hassio from a snapshot.
