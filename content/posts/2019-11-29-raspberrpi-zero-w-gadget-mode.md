---
title: Using Raspberry Pi Zero W in Gadget Mode On a Linux Laptop 
date: 2019-11-29
tags: ['rpi', 'raspberrpi', 'linux']
---

# Raspberry Pi "Gadget" mode 
In my previous [post](/post/2019-10-11-tasmota-bn-link-bnc-60) I went over how to get tasmota on a 
BN Link BNC 60 smart outlet using a Raspberry Pi.  When I started down that journey I tried to use
Gadget mode on the RPI so I did not have to use a keyboard/monitor but I had issues.  Turns out my
issue was actually just that the USB cable I was using was a data only cable, so into the trash
that cable went.

Gadget mode is basically a way to run your RPI off your laptops USB port.  It allows power over USB,
as well as data over USB so you can ssh into the RPI and start using it.

## How to
Setting up gadget mode is pretty easy and has been covered a bunch online.  The best guide
is [here](https://blog.gbaman.info/?p=791), but the TLDR is:

1. Install Raspbian onto SD card.
2. Mount boot partition of that SD card and cd to it.
3. Add `dtoverlay=dwc2` to `config.txt`
4. `touch ssh` to enable ssh
5. Add `modules-load=dwc2,g_ether` after `rootwait` to `cmdline.txt`, be careful with this file,
   it's very picky about formatting.
6. Eject the SD card, put it in your RPI and plug the RPI into your computer.

Note: If you are using a RPI Zero like I am make sure the USB cable is plugged into
the USB port on the RPI, not the PWR port.  The RPI can get data and power off the USB port.

# How to get "internet Sharing" setup in Linux

A lot of guides out there talk about how to setup "internet sharing" on Windows
Or MacOS.  Internet sharing allows your RPI, which is now connected to the USB of your
computer to share the internet access your laptop is using.

I'm using a flavor of Ubuntu called KDE Neon, however anything that uses `network manager`
should work the same way.  Basically all you need to do to allow your RPI to connect to
the internet using your computers network connection is:

* Open Network settings (Connections in KDE Neon)
* Find the "Wired Connection" that corresponds with your RPI RDNIS device.
* Click the "IPv4" tab and change the "Method" to "Shared to other computers"

Now give your computer a few seconds to assign it a proper IP address.  Then you can run 
`ifconfig` to see what the newly assigned IP is.  Or just ping `raspberrypi.local` (assuming
you have mdns setup).

Now when you ssh into the device, your RPI should be able to connect to the internet,
try it out with `curl -v https://www.example.com`
