---
title: Bitcoin Heater Financial Viability
date: 2023-11-22
tags: ['bitcoin', 'heater', 'savings']
---

Recently I was moved to a place that has cooler temperatures and heard about using an old ASIC
miner to heat.  This post explains the financial viability of using an old ASIC miner as a space
heater.

## Bitmain/Antminer S9 ASIC
[Crypto Cloaks](https://www.cryptocloaks.com/product/s9-bitcoin-space-heater-bundle/) has a really
good guide and bundle to get started with using an S9 as a space heater.  Lets look at the financial
viability if you were to buy their bundle compared to a space heater from a local store.

At the time of writing this post the Crypto Cloaks bundle is listed for $259, that includes
the ASIC, but not the replacement fans that make the ASIC quiet.  The Noctua fans are an additional
$100.  So we are looking at a total of $359, lets round that up to $360.

The S9 uses around 1300w, or 31.2 kWh of power a day.

## The competition (A Regular Space Heater)
You can get a 1500w space heater from your local 
[walmart](https://www.walmart.com/ip/Hyper-Tough-Black-1500w-Heavy-Duty-2-Setting-Electric-Utility-Space-Heater/336545728)
, target, home depot, etc. for around $30, sometimes even cheaper. 

$30 is a lot cheaper than $360, but the $30 space heater won't make you any returns, but does that matter?

Most of these heaters are 1500w, or 36 kWh of power a day.

## Heaters Compared
Where I live the electricity rates for the winter are between 8.0390 and 9.8154 cents.  To make things
easy lets just pick the middle number: 8.8627 cents per kWh.

We are only going to run the heaters during the winter (September-May), or 7 months.

Lets see how much each of the options above costs us per year.

* S9 ASIC: 31.2 kWh * 8.8627 / 100 is $2.76 USD per day, or $82.95 a month.
    * $82.95 * 7 =  $580.68 to run the ASIC for the winter.
    * According to [this website](https://www.asicminervalue.com/miners/bitmain/antminer-s9-13th) 
      we would make about $29.64 in BTC a month. So 28.64 * 7 = $200.48
    * Our net for the winter would be 580.68 - 200.48 = $380.20
* 1500w space heater: 36 kWh * 8.8627 / 100 is $3.19 USD per day, or $95.71 a month.
    * $95.71 * 7 = $670.02 to run the heater for the winter.


Here is the equation for how much it will cost us to run the 1500w space heater vs the ASIC Bitcoin
minder: `670.02 - (580.68 - 200.48) = 289.82`.  So If we decide to use the ASIC Bitcoin as a space
heater it will save us $289.82 every winter, or about $41.40 per month if we run it for 7 months 
in the winter.  Our ROI on the initial $360 we paid for the ASIC miner is about 9 month.  Not
a bad ROI, run it for one full winter and just a few month in the next winter and you will be
set to make some sats.

## TLDR
Going with the Crypto Cloaks s9 Bitcoin space heater will pay for itself in about 9 months.

Assumptions:

* S9 ASIC miner price: $360
* 1500w space heater price: $30
* Electricity cost: 8.8627 cents per kWh.
* Only run the heaters for 7 months during the winter.

## Further Thoughts:
* What is the opportunity costs of putting ~ $330 into a ASIC miner rather than inviting it?
* What is the ROI if you buy the s9 miner, fans, 3d printerd case and esembly yourself?
* The 1500w heater is simplier and brand new, I assume it will last longer than the ASIC miner.
