---
title: Bitcoin Node Hardware Options
date: 2025-03-06
tags: 
    - bitcoin
    - homelab
    - proxmox
    - self-hosting
    - blog
---

This post outlines my thoughts on hardware options to run a Bitcoin node at home.

* Buy a pre-built node from a company like start9.
* Build a node using a thin client
    * Dell Wyse 5070
    * Dell Wyse D90d7
    * HP T640
* Build a node using a Tiny Mini Micro PC.

## Pre-built node
If you are not very technical and you are okay with spending some more money and getting support,
I would recommend a pre-build node from a company like start9.

## Thin Client
Thin clients are small form factor computers that you can get used for a good price.
Another big upside of these are that most are fanless and have low power consumption.

[Parky Towers](https://www.parkytowers.me.uk/thin/) is a great resource for thin clients.

### Dell Wyse 5070

![Dell Wyse 5070](5070_450.webp)

This is the thin client I currently have.  I have Proxmox installed on it, and my Bitcoin Node
running as a VM inside of Proxmox.

The 5070 has an m.2 slot that you can populate with a sata SSD, but not an NVMe drive. Sata m.2
SSDs are more rare than NVMe drives, they are slower, and they are about the same price.
For this reason, I would recommend the HP T640 over the 5070.  The Dell Wyse D90d7 should work in
theory, but the hardware is slower and older than the 5070.

Here is a screenshot from [diskprices.com](https://diskprices.com/) showing the price of m.2 sata
compared to m.2 NVMe drives, and 2.5" sata. As you can see, a 2tb drive is about the same between
all 3 options.  However the NVMe drive will be much faster than Sata.

![disk prices](diskprices.png)

### Dell Wyse D90d7
This is the thin client I would have gone with if I had known about it before I bought the 5070.

I found [this blog](https://thunderysteak.github.io/wyse-d90d7-ssd) that influenced my decision to
look into the D90d7.

Compared to the 5070, the D90d7 has a slower CPU, and slower RAM.  The D90d7 has a normal
sata slow, so with an adapter you can use a 2.5" SSD.  In my experience, 2.5" SSDs are cheaper,
more common, but the same speed as m.2 sata SSDs.

I assume that a Bitcoin initial Bitcoin sync would take longer on the D90d7 than the 5070, but
I have not tested this.

The Dell D90d7 is older and therefore would be the cheapest option in this list.

### HP T640

![HP T640](t640_450.webp)

The HP T640 is a thin client that accepts a m.2 NVMe drive.  Accepting an NVMe drive
allows faster storage that is also more future proof than the m.2 sata drives.  The T640
also will accept a m.2 sata if that is all you have. However In my experience, m.2 sata drives
are about the same price as m.2 NVMe drives.

The T640 is newer, faster, and more expensive than the other thin clients, but if you
have the money, I would recommend it.

## Tiny Mini Micro PC
Tiny Mini Micro PCs are small form factor computers that are usually more powerful than thin clients.
Compared to thin clients they are no fanless, they usually use more power, but have more features.
The main feature that TMM PCs have that could be a big benefit is Intel ME.  Intel ME allows you
a cheap way to remotely manage the computer.  However not all TMM PCs have Intel ME.

If you are okay with a louder computer, that uses more power, but has more features, and you
are okay spending a bit more money, I would recommend a TMM PC.

## Conclusion
I'm a technical person so building a DIY node is what I'm doing.  I also like quiet, power efficient
machines so the thin clients make more sense for me.  What do you think, how do you run your Bitcoin
node?
