---
title: State of My Homelab 2024
date: 2024-12-27
tags: 
    - k8s
    - homelab
    - kubernetes
    - proxmox
    - self-hosting
    - blog
---

I've had a homelab in some form for a couple years, it has evolved over time.
This post will go over what the current state of my Homelab is as of the end
of 2024.

## Hardware
My Homelab consists of 4 "servers".  I call them servers but they are really 
small form factor business computers.  More specifically I have:

* 2 Lenovo P330 Tiny's.
    * 1 has 64GB of DDR4 memory, 1 1TB 2.5 sata SSD, and an i7-8700T
    * 1 has 32GB of DDR4 memory, with 2 1TB M.2 NVMe SSDs, and an i7.
* 1 HP Elitedesk 800 G4 SFF
    * Dual 128GB M.2 NVMe SSDs in a Raid1 array for the OS.
    * Dual 12TB 3.5 inch HDDs in a Raid1 array for bigger storage. 
    * 24GB of memory (8GB * 3)
    * Intel i5-8500
* 1 Dell Wyse 5070 Thin Client.
    * Intel Celeron J4105
    * 20GB of memory
    * 1TB M.2 Sata SSD.


![Alt text](servers.jpeg "Servers")

Currently all of the machines have 1Gbps, but I plan to upgrade above that in the near future.

I used to have 3 HP 800 G3 Mini's, but I recently replaced them with the Lenovo and HP SFF.
The reason I picked the Lenovo P330 tiny's is because they support dual M.2 NVMe Drives,
and they have a x8 PCIe slot.  I bought the HP SFF because it supports 2 3.5 inch drives and I
was looking to host larger files.

I was able to pick up the Lenovos for about $200 USD, and the HP SFF for $100 USD on Ebay.

The Dell Wyse 5070 I got to test out how a fanless thin client would work as a server.  I've
been very happy with it so far, however I don't really need it anymore and mostly just use
it for to run a Bitcoin node.

### iGPU passthrough
I have the iGPU of the Lenovo P330 tiny's passed through to the k8s cluster. This allows me to 
do video transcoding with Jellyfin.

### Perfection ( or lack there of )
You might notice some weirdness with the memory configuration. Ideally I would have the Lenovos
with matching memory, but I haven't found a real need to spend the money to upgrade the 32GB to 64GB.
I ordered 32GB of memory for the HP SFF, but one of the sticks was the wrong kind, so I ended up with
24GB of memory.

One of the Lenovos has dual NVMes in a raid array.  This is because I had a drive fail on me, and it
was a pain to restore from backups. I decided to use raid1 so I wouldn't have to deal with a similar
event in the future.  I haven't spent the money to get 2 more NVMes for the other Lenovo.

I bring up the memory weirdness, and the lack of raid1 NVMes in the second Lenovo
because I think it's important to show that you don't need to have everything perfect to have a
homelab.  I could probably get by with just a Dell Wyse 5070 with a bunch of containers.

## Software
All of these servers have Proxmox installed on them, they are clustered together.  They are also
setup to use ZFS for the storage.  ZFS and clustering allows me to move VMs around easily; this
is especially helpful to apply updates.

I also have a k3s cluster running in VMs on the Proxmox cluster.  I have 3 master nodes and 3 worker
nodes.  I'd like to have just 2 worker nodes, but I'm not 100% sure if all my workloads will work
with just 2 worker nodes.

## Workloads
I have a mix of workloads running in my homelab but the plan is to slowly move things into the k3s
cluster and only keep a few things outside of Kubernetes.

### Virtual Machines
* nfs-1 - nfs server that provides the 12TB storage to other containers/VMs.
* k3s-master-yy88
* k3s-master-t4r8
* k3s-master-8wz6
* k3s-worker-4f8d
* k3s-worker-7t1j
* k3s-worker-ff4s
* nvr-c850 - Xpenology NVR.
* nas-lg7a - Xpenology NAS.
* pbs-1 - Proxmox Backup Server.
* Mynode-btc-mainnet - Bitcoin node.
* win11 - Windows 11 VM that I rarely use.
* mynode-btc-testnet - Bitcoin node running in testnet mode.
* podman-1 - testing podman.
* minecraft-2 - Minecraft server I need to get back to deploying.
* grafana-1 - Old Grafana server that I can decom.
* adguard-1 - Adguard Home server that provides DNS level ad blocking.

### Linux Containers
And here are the containers
* docker-1 - Docker host that runs various containers.
    * Portainer
    * Gatus
    * Freshrss
    * Traefik
    * Actual Budget
    * Homer
    * Memos
* adguard-backup - Adguard home backup server.
* cloudflare-0 - Provides cloudflare tunnel as an alternative to my VPN.
* cloudflare-1 - 2nd cloudflare tunnel node so it's HA.
* bitcoin-testnet - an old Bitcoin testnet node, can be decommed.
* ups-nut-0 - NUT server that monitors my UPS.
* prometheus-0 - Old prometheus server that can be decommed.

### Kubernetes Workloads
* ArgoCD - GitOps tool I use to deploy all k8s workloads.
* kube-prometheus-stack - Monitoring stack.
* cert-manager - Provides TLS certificates for my services.
* ingress-nginx - Ingress controller.
* kube-vip - Provides a floating IP for the k8s master nodes.
* metallb - Provides a floating IP for the k8s service of type load balancer.
* authelia - Provides SSO for my services.
* bitwarden - Provides passwords for k8s workloads.
* cloudflare-ddns - Updates my cloudflare DNS records.
* Cloudnative-PG - Provides Postgres database for various workloads (authelia, radarr, sonarr, etc).
* external-secrets-operator - works with Bitwarden to provide secrets to k8s workloads.
* *arr stack for media management.
* Jellyfin - Media server.
* Jellyseer - Nice interface to get media for Jellyfin.
* intel-gpu-plugin - Provides GPU access to containers.
* lldap - ldap server that works with Authelia.
* openebs - Provides storage for k8s workloads.
* redis-operator - provides redis for k8s workloads, currently only used by authelia.

## Future improvements
I'd like some higher speed NICs for the Lenovo and HP SFFs.  I've seen others mention
Mellanox ConnectX-3 cards that can be had for less than $50 on Ebay.

I'd also like to move most of the workloads off the Proxmox VMs and lxc containers into
the k3s cluster.

The 2 biggest resources hogs in the k3s cluster is ArgoCD and kube-prometheus-stack. Exploring
Flux as an alternative to ArgoCD would be nice. I'd like to look at using Victoria Metics to 
replace kube-prometheus-stack.  Mainly I want to see if I can reduce the resource usage.

There is also a long list of small things I need to do such as:

* Upgrade PBS
* Update intel-gpu-plugin so I can share the iGPU with more than 1 container.
* Get the NUT server setup to shutdown all the servers with the UPS is low.
* Setup better alerting for bad drives, failed backups, etc.
* Extend kube-vip to replace metallb.
* Upgrade k3s cluster.
* Get more metrics into prometheus.
    * Proxmox metrics
    * Openebs metrics
    * Cloudnative-PG
    * etc

## Conclusion
I'm happy with the current state of my homelab.  I've been able to self-host a lot of services,
and the system is fairly resilent.  I'm looking forward to the future improvements I have planned.
If you have looking into starting a homelab, I'd recommend starting small.  Pick up a cheap
thin client like the Dell Wyse 5070 and start playing around with infrastructure.
