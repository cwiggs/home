---
title: 2019 Oregon trip 
date: 2019-07-07
tags: ["travel", "oregon", "life"]
---

We just got done with a trip to Oregon and it was beautiful.  Oddly enough it only rained a tiny bit at the end of the trip.

# Albany
Our first stop was in Albany, OR to get some pizza at a local place called [ciddici's](http://www.ciddicipizza.com/).
There are only 2 locations and they are both in Albany.  The pizza here is interesting for 2 reasons:  One, the crust
is fairly crispy, but it compliments the rest of the pizza well. The second reason is the cup-life pepperonis.

# Newport / Otter Rock
Our 2nd stop was in Newport were we checked out the Yaquina Head lighthouse:

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/cwiggs/48228056992/in/dateposted-public/" title="Yaquina head lighthouse"><img src="https://live.staticflickr.com/65535/48228056992_99f41ba7ec_c.jpg" width="800" height="600" alt="Yaquina head lighthouse"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

Then we stopped at the pier and had dinner at the Mo's Seafood and Chowder.
<a data-flickr-embed="true"  href="https://www.flickr.com/photos/cwiggs/48227930357/in/dateposted-public/" title="Newport pier"><img src="https://live.staticflickr.com/65535/48227930357_5ab36e69ed_k.jpg" width="800" height="600" alt="Newport pier"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

We ended the night with checkout out the The Devil's Punch Bowl.

<a data-flickr-embed="true"  href="https://www.flickr.com/photos/cwiggs/48228037122/in/dateposted-public/" title="Devil&#x27;s punch bowl"><img src="https://live.staticflickr.com/65535/48228037122_a0711b46b2.jpg" width="800" height="600" alt="Devil&#x27;s punch bowl"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

# Other Places
After Newport & Otter Rock we drove up the cost and saw the tillamook factory, Cannon Beach (famous from the movie the Goonies), and stayed in Long Beach, WA before heading back to Portland.

Watch out for more posts about the rest of the trip.
