---
title: Bitcoin Node Software Options
date: 2025-03-06
tags: 
    - bitcoin
    - homelab
    - proxmox
    - self-hosting
    - blog
---

# Bitcoin node software
If you want to run a bitcoin node yourself there are various options out there.
A popular choice is what I'm calling "Bitcoin Node Software".  BDS is a collection of software
that makes running your own Bitcoin node easier, or more user friendly.  Some options are:

- [Umbrel](https://umbrel.com/)
- [MyNode](https://mynodebtc.com/)
- [Start9](https://start9.com/)
- [RaspiBlitz](https://docs.raspiblitz.org/)

There are a few others out there but I'm going to go over the above 4 since that is what I have experience with.

## What I'm looking for
I run my Bitcoin node in my homelab on Proxmox.  I think my situation is fairly uncommon, as a lot
of the options you can buy out there for running a Bitcoin node are designed to run on a single system.
Here are the things that I want in a Bitcoin node software:

- Ability to store the blockchain on a separate drive.
- Ability to install on top of Linux.  Mainly so I can run qemu-guest-agent.
- Easily run testnet

## Umbrel
Umbrel is probably the most popular option out there.  However recently they seem to be re-branding
as an OS for Homelab/Self-hosting instead of focusing on Bitcoin.

In previous verions of Umbrel you could run Bitcoin core with a separate data directory for the blockchain.
However as of Umbrel 1.0 that is no longer an option. 

I believe most of the Umbrel "apps" are just docker containers. I prefer this over bash scripts that just install
the app, sometimes with systemd services.

### Pros:
* Ability to run testnet3 (maybe newer versions will have the ability to run testnet4).
* Uses docker containers for apps.

### Cons:
* Can't store blockchain on separate drive.
* Can't install on top of Linux.
* Rebranding as a self-hosting OS, moving away from Bitcoin?

## MyNode
Next I tired MyNode.  I really like the UI of Mynode and the ability to install on top of Linux.
However If found a bug while installing on top of Linux.  I was able to submit a fix and the dev
did respond quickly and merged the fix.  I think bugs like this are more common when you are using
bash scripts.

### Pros:
* Ability to store blockchain on separate drive.
* Ability to install on top of Linux.
* Nice UI.
* Can run testnet3.

### Cons:
* Lots of premium features. Certain apps you can't use unless you pay.
* Lots of bash/shell code.  I'm not a fan of this, these shell scripts break easily.

## Start9
The next option I tried is Start9.  I've seen multiple videos online from the creater of Start9
and I appreciate a lot of his views, and that he is more involved in the Bitcoin community.

I did not find a way to install Start9 on top of Linux, however qemu-guest-agent is installed and running
by default.

### Testnet support
In the past I searched and found [this github](https://github.com/Start9Labs/start-os/discussions/2603)
issue that talks about testnet support, however the feature has not been implemented.

While searching again I found this [app](https://github.com/remcoros/bitcoind-testnet4-startos).
I installed it in start9 and it works great.  However this app is for testnet4, and most other apps
do not support testnet4 yet.  I would love to see more testnet4 support added in the future.

### btrfs
Start9 has the ability to store the blockchain on a separate drive but because it uses btrfs,
it wasn't as easy as mounting the vdisk and move data around on that disk.  Instead I had to
copy the data from the old vdisk to the new vdisk that has btrfs on it.

I would be curious what the down-sides are of using btrfs for the blockchain data directory.
I think there might be upsides as well, but I don't know enough about btrfs to say.

### Pros:
* Uses Docker containers for apps.
* Nice UI
* Docs on creating a new "app" are good.
* Uses btrfs for the blockchain data directory.
* Ability to store blockchain on separate drive.

### Cons:
* No native testnet support.
* No ability to install on top of Linux.

## RaspiBlitz
RaspiBlitz was the next one I tried. I was able to install it on top of Linux.
There was an option for testnet3, more info [here](https://github.com/raspiblitz/raspiblitz/pull/4579)
However I had to start bitcoin in mainnet mode first and then run the testnet script.
The script is written in bash and is not setup to fail properly, so it took some debugging
to figure out that I needed to run bitcoind in mainnet mode first.

### Pros:
* Ability to store blockchain on separate drive.
* Ability to install on top of Linux.

### Cons:
* No testnet4 support.
* testnet3 support is not easy to setup, prone to errors.
* Lots of bash/shell code.  I'm not a fan of this, these shell scripts break easily.

## Conclusion
I hope this list helps others picking software to run their Bitcoin node.
IMO none of these are perfect, but I think I'll stick with Start9 for now,
maybe try to add more testnet4 apps.
