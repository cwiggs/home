---
title: Install Tasmota OTA on BN-LINK BNC-60 
date: 2019-10-11
tags: ['iot', 'tasmota', 'home-automation']
---

# Flash Tasmota
So it seems there are a few ways to flash Tasmota onto devices.  An over-the-air way is via a tool called [tuya-convert](https://github.com/ct-Open-Source/tuya-convert) which is what I used.

You can use any linux device that has a spare WIFI adapter to run the software.  I have a few Raspberry Pi Zero W's sitting around that work great.  At first I tried to put the RPI into "gadget mode" using [this](https://gist.github.com/gbaman/975e2db164b3ca2b51ae11e45e8fd40a) guide.  It ended up not working for me, however I think it was just because of a bad USB cable.  So instead of using gadget mode I found my mini-hdmi to hdmi cable, and micro USB to regular USB typeA, and I hooked up the RPI to a monitor and keyboard and flashed [Rasbian](https://www.raspberrypi.org/downloads/raspbian/) to the Micro SD card and booted it up.

# tuya-convert
Now that you have a running linux system, lets login and get things setup.  The default login for raspbian is `pi:raspberry`, it's highly recommended to change the default credentials.

Now that we are logged in Lets setup wifi.  You can use the simple `sudo raspi-config` command to edit most settings
for you RPI.  Run the `raspi-config` command go to `Network Options` and then `Wi-fi`, fill out all the info and you should be able to connect to the internet.

Now lets git clone and run tuya-convert:
```
git clone https://github.com/ct-Open-Source/tuya-convert
cd tuya-convert
./install_prereq.sh
sudo kill wpa_supplicant # if you don't run this command the Wifi AP will not work.
# Plug in the BN-LINK device and press and hold the power button
./start_flash.sh
```

Once the `start_flash.sh` script runs it create an access point for the BN-LINK device to connect to and look
for a file to flash.  Once the script is done you can run `curl http://10.42.42.42/flash3` to flash Tasmota.

# Tasmota
Now that we have Tasmota flashed onto the BN-LINK device we need to get it on our network.
The BN-LINK device, now with Tasmota should be broadcasting a WIFI network named something like `sonoff-2133`.
Connect a device (like a phone) to that WIFI network and browse to `192.168.4.1`.  You are now connected to the
Tasmota device, enter in your WIFI info, click save and the device will restart, and connect to your WIFI network.

You can now check your router (or DHCP server) for the IP address for the Tasmota device (it should have a name with `sonoff` in it).

The last thing to do is to browse to Tasmota and use a template to configure the device:

* Browse to your device via IP in the browser.
* Click Configuration > Configure Other.
* In the Template box enter `{"NAME":"BNC-60/U133TJ","GPIO":[0,56,0,17,134,132,0,0,131,57,21,0,0],"FLAG":0,"BASE":18}
`
* Check the `Activate` button and slick Save.

Now your device will have all the settings for the BN-LINK (like power monitoring!)

# Setup Power Monitoring
These BN-LINK devices are not just a basic smart outlet, they also have power monitoring.  You'll need to setup
the power monitoring for it to work correctly.  You can find out how to do that [here](https://github.com/arendst/Sonoff-Tasmota/wiki/Power-monitoring-calibration).  But the TLDR is:

* Grab a lightbulb and make note of how many Watts it uses.
* Plug in a lightbulb and turn the smart outlet on.
* Go to the console (in the webui of Tasmota) and enter `PowerSet <wattage of lightbulb)`
* Use a multimeter (or assume 120 Volts if in the US) to measure the Voltage of the outlet.
* Go to the console and enter `VoltageSet <voltage>`. e.g. `VoltageSet 120`
* Take your watts and device by the volts to get the amps.  e.g. 9.5/120 = 0.07916666666
* Times the amps by 1000 (0.07916666666 * 1000 = 79.16666666
* Go to the console and enter `CurrentSet 79.16666666`
* Now when you turn on the light the Power Factor should be 1 or very close to 1.

Basically you just put your Watts, Volts, and Amps into the device so it measures everything correctly.

# Sources
* https://www.raspberrypi.org/documentation/configuration/wireless/wireless-cli.md
* https://github.com/ct-Open-Source/tuya-convert
* https://blakadder.github.io/templates/bn-link_bnc-60.html
* https://forums.homeseer.com/forum/lighting-primary-technology-plug-ins/lighting-primary-technology-discussion/mcsmqtt-michael-mcsharry/1291252-energy-monitoring-smart-plug-for-under-10
* http://mcssprinklers.com/mcsMQTT.pdf
