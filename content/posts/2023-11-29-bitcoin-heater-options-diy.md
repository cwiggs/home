---
title: Bitcoin Heater Financial Viability part 2 (DIY)
date: 2023-11-29
tags: ['bitcoin', 'heater', 'savings', diy]
---

In my previous post about how financially viable is a Bitcoin ASIC miner as a space heater
I talked about buying the s9 miner bundle from [Crypto Cloaks](https://www.cryptocloaks.com].
Their setup is good if you aren't ready to diy but lets take a look at the financials behind diy.

## Parts
* Bitmain/Antminer S9: I've seen these on ebay for around $120.
* 2 [140mm Noctua fans](https://a.co/d/3EzwM5p): $60
* 1 [60mm Noctua fan](https://a.co/d/3RrbYJG): $15
* (optional) 3d print the Crypto Cloaks case at a local library or hackerspace.
* Total: $195

## Updated Comparison 
So we are looking at a total of $195 instead of $360.  That would bring our ROI down to
about 5 months, not bad.

There are a few things that you have to keep in mind:

* This assumes you are comfortable soldering the correct connector on the 60mm fan, if not
  you can buy an adapter.
* This assumes you can plug the miner into ethernet, otherwise you will have to buy a wifi
  adapter for it.

## Further Thoughts:
* By running a Bitcoin mining node you are helping to secure the Bitcoin network.
* Running a Bitcoin mining node is a good way to get KYC sats.
* What is the opportunity costs of putting ~ $165 into a ASIC miner rather than inviting it?
* As ASIC miners decrease in cost, and Bitcoin increases in cost it will make even more financial
  sense to use an ASIC miner instead of a space heater.
