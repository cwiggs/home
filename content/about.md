---
title: About me
comments: false
---

## About
Hello, I'm Chris Wiggins, Currently I'm an infrastructure engineer building the tech infrastructure to enable companies to run,
scale, etc.  Previously I was a site reliability engineer, systems engineer, and Linux administrator.

I have a passion for technology, and I'm always looking to learn new things.

I maintain a homelab, and you should too!
{{< article link="/posts/2024-12-27-state-of-homelab/" >}}

## Facts about me.
* Vim is my editor of choice, even as a full blown IDE.
* I run Debian testing as my main Linux Distro.
* I'm a huge fan of the command line.  Give me a TUI over a GUI any day.

## Opinions
* Linux > Windows (except maybe Active Directory).
* Open-source runs the world and is awesome, everyone's life would be much worse without it.
* Vim > Nano

## Skills
* Containers
    * Kubernetes
    * Podman
    * Docker
    * LXC
* Public "Clouds"
    * Amazon Web Services (AWS)
    * Oracle Cloud (OCI)
    * Google Cloud (GCP)
    * Microsoft Azure
* Linux
    * awk ( I'm the guy finds a grep, sed, cut and replaces it with awk )
    * grep
    * tmux
    * bash
    * systemd
    * etc
* Programming Languages
    * Python
    * Groovy
    * Go
* CI/CD Systems
    * Jenkins ( I've written way too much Groovy, don't make me write any more please. )
    * Gitlab CI
    * Github Actions
    * Circleci
    * Drone CI
* Many other things I forgot to list or have learned since I updated this page.

## Hobbies
* Technology
* Bicycling
* Outdoors
* Philosophy

## Contact
* Email: me at cwiggs.com
